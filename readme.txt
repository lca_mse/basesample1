@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: Sample1
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-12-30
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ----------------------------------------------------------------------
@REM #######################################################################*/

Eclipse, Maven, Git,...

How it all works together.

Base Sample

See bin\




Maven und Eclipse Tutorial

Einmalig f�r den Workspace

    Eclipse: Eclipse Workspace anlegen
    CMD: Eclipse Workspace f�r Maven initialisieren: Im Workspace: mvn -Declipse.workspace=%CD% eclipse:add-maven-repo



Pro Projekt

    CMD: Make new (Simple) Project with Maven: mvn archetype:generate -DgroupId=com.logicals.app -DartifactId=ProjectName -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
    CMD, Im Project: : Make Eclipse Files for Project:mvn eclipse:eclipse
    Eclipse:File->Import->General:ExistingProjectIntoWorkspace->Next->RootDirectory==ProjectName->Finish
    Eclipse: Popup des Projects -> RunAs -> JavaApplication -> App com.logicals.app
    add .gitignore file
    Initialize for .git
    Add Repo
    ...

Durch 3) werden die folgenden Objekte im Projekt erzeugt:

    src\main\java\com\logicals\app\App.java
    src\test\java\com\logicals\app\AppTest.java
    pom.xml

Durch 4) werden die folgenden Elemente erzeugt:

    .classpath
    .project
