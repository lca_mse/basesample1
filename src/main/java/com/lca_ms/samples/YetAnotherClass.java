package com.lca_ms.samples;

class YetAnotherClassBase
{
	public YetAnotherClassBase()
	{
		mAuthor = System.getenv("LC32_AUTHOR");
	}
	public String getAuthor()
	{
		return mAuthor;
	}
	private String mAuthor;
}


public class YetAnotherClass
extends YetAnotherClassBase
{
 public String getAuthor()
 {
	 return "<"+super.getAuthor()+">";
 }
}
