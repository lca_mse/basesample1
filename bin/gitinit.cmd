@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: first time git init and upload to repo
@REM ##
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-12-30
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ----------------------------------------------------------------------
@REM #######################################################################*/
@echo off
setlocal

call :getP %CD%
echo --%_CURRDIRNAME%--

call git init
echo call git remote add origin ssh://git@bitbucket.org/lca_mse/%_CURRDIRNAME%.git
call git remote add origin ssh://git@bitbucket.org/lca_mse/%_CURRDIRNAME%.git
call git add pom.xml
call git commit -m "first put"
call git push -u origin --all

goto :ende


:getP
set _CURRDIRNAME=%~n1
:ende
endlocal

