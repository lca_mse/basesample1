@REM #########################################################################
@REM ## Copyright (c) logi.cals. All rights reserved.
@REM ## ----------------------------------------------------------------------
@REM ## TASK      :: put sources (after commit)
@REM ##              to be called inside git repository (e.q. custom-log-src)
@REM ## ----------------------------------------------------------------------
@REM ## AUTHOR    :: Mario Semo
@REM ## CREATED   :: 12-08-07
@REM ## ----------------------------------------------------------------------
@REM ## NOTES     :: none
@REM ## ======================================================================
@REM ## HISTORY   :: $Author: $$Revision: $
@REM ##              $Date: $
@REM ## ----------------------------------------------------------------------
@REM ## $Log: $
@REM #######################################################################*/
@echo off
setlocal

if (%1)==() goto error

call git add .
call git commit -m %1
call git push origin master
goto :ende

:error
echo need put comment as arg1
goto :ende

:ende
endlocal
